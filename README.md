# Foodo
Food Donation App

# Proposed System 

   The application ‘FooDo’ will first ask the user if they just want to donate some food (if they are a donor) or if they want to login as a volunteer (if the user is a volunteer). A user can also know more about the app by clicking on the ‘about us’ button.
  
  If the user is a volunteer, (s)he will have to login. Once (s)he logs in, they will be alerted of a new donation that requires a volunteer. If (s)he chooses to accept the delivery, (s)he will be asked to enter basic information, like his/her name, mobile number and current location. Once (s)he reaches the location of the donation, (s)he will have to check the quality of food and verify the quantity available. Once (s)he has delivered the food to the NGO, they will click on a button to confirm the completion of the delivery. If the user however is a new volunteer, (s)he can register by entering email-id, a password and his/her mobile number after which (s)he can login using the login page.
  
  If the user is a donor, the application will ask him/her to enter their full name, mobile number, address, type of food they are donating ( veg/ non-veg), and quantity (in number of people it will serve). After confirmation of the details, the donor will be taken to another page which will show the information about the pickup. The volunteer coming to pick up will also perform a quality check of the product so as to ensure the food delivered is in good condition. If the donation is verified, it is delivered to an NGO and the donor will get a notification and a thank you message. 

# Activities
1. HOME PAGE

This is the page that will open the moment you click on the application. 
This page will have three buttons: 1.Donate  2.Login   3.About us

2. DONATE 

After clicking on the donate button on the homepage user will be taken to this page. Here the user will have to enter the details about the donation he/she is about to make. It is compulsory for the user to enter the following details for smooth pickup of the donation:
* Full name of the donor
* Mobile number of donor
* Address 
* Type of food ( Veg/ Non-veg)
* Quantity of the donation ( in number of people it would feed)

Once the user is done he/she can click on the button “Donate”  to go to the next page.